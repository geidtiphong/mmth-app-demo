import { BaseRequestModel, BaseViewModel } from "./base-request-model";

export interface OrganizeViewModel extends BaseViewModel {
  OrganizeId: number;
  OrganizeName: string;
}

export interface OrganizeViewModelResponse extends BaseRequestModel {
  Entity: OrganizeViewModel[];
}
