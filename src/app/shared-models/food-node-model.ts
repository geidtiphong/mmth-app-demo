export interface FoodNode {
  Name: string;
  MainName?: string;
  Link?: string;
  Children?: FoodNode[];
  ProgramCode?: string;
  LinkProgramCode?: string;
}
