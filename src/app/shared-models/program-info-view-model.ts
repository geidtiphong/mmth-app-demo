import { BaseResponseModel } from "./base-response-model";

export interface ProgramInfoViewModel {
  PROGRAM_CODE: string;
  PROGRAM_NAME: string;
  PROGRAM_LINK: string;
  ORGANIZE_ID: number;
  ROLE_CODE: string;
}

export interface ProgramInfoViewModelResponse extends BaseResponseModel {
  Entity: ProgramInfoViewModel;
}
