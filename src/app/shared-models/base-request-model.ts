export interface BaseViewModel {
  ORGANIZE_ID?: number;
  ORGANIZE_NAME?: string;
  CREATED_DATE?: string;
  CREATED_BY?: string;
  UPDATED_DATE?: string;
  UPDATED_BY?: string;
  TOTAL_RECORDS?: number | string;
  TOTAL_PAGES?: number | string;
  IS_CHECKED?: boolean;
  USER?: string;
  ROWS_CHECKBOX?: number;
}

export interface ResponseExcel extends BaseRequestModel {
  Entity: BaseFileModel;
}

export interface BaseFileModel {
  FILE_KEY?: string;
  FILE_NAME?: string;
  FILE_PATH?: string;
  FILE_SIZE?: string;
  FILE_DATE?: any;
}

export interface BaseRequestModel {
  Entity?: any;
  Errors?: any;
  IsAuthenicated?: boolean;
  ReturnMessage?: any;
  ReturnStatus?: boolean;

  CurrentPageNumber?: number;
  CurrentPageIndex?: number;
  PageSize?: number;
  SortDirection?: string;
  SortExpression?: string;
  TotalPages?: number;
  TotalRows?: number;
}
