import { BaseResponseModel } from "./base-response-model";

export interface ResponseSelectbox extends BaseResponseModel {
  Entity: valueSelectbox[];
}

export interface valueSelectbox {
  KEY: string;
  VALUE?: string;
  IS_CHECKED?: boolean;
}
