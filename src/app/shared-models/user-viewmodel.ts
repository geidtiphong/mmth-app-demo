import { BaseRequestModel } from './base-request-model';

export interface UserViewModel {
  CompanyName?: string;
  FirstName?: string;
  IsAuthenicated?: boolean;
  LastName?: string;
  OrganizaId?: number;
  OrganizeName?: string;
  RoleCode?: string;
  UserName: string;
  UserId?: number;
  Token?: string;
  TokenExpirationDate?: Date;
  Password?: string;
  PasswordConfirmation?: string;
}

export interface ResponseUserViewModel extends BaseRequestModel {
  Entity: UserViewModel;
}