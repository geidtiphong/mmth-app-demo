import { BaseRequestModel } from './base-request-model';

export interface RoleViewModel {
  RoleCode: string;
  RoleDescription: string;
}

export interface RoleViewModelResponse extends BaseRequestModel {
  Entity: RoleViewModel[];
}