import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { SessionService } from "../shared-core-app/main-core-shared";

@Injectable({
  providedIn: "root"
})
export class AuthGuardService implements CanActivate {
  constructor(private router: Router, private sessionService: SessionService) {}

  canActivate() {
    if (this.sessionService.isExpiredSession()) {
      window.alert(`You are not authorization !`);
      this.router.navigate(["/login"]);
      return false;
    }
    return true;
  }
}
