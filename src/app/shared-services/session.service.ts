import { Injectable } from "@angular/core";
import { JwtHelperService } from "@auth0/angular-jwt";
import { AppSettings } from "../shared-models/appsettings.model";
import { UserViewModel } from "../shared-models/user-viewmodel";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class SessionService {
  appSettings: AppSettings;
  userViewModel: UserViewModel;
  isAuthenicated: boolean;
  MAIN_PROGRAM_NAME: string;
  PROGRAM_NAME: string;
  PROGRAM_CODE: string;
  SHOW_BREADCRUMB: boolean;
  apiUrl: string;
  baseApiUrl: string;

  jwtHelperService = new JwtHelperService();

  constructor() {
    this.MAIN_PROGRAM_NAME = "Loading...";
    this.PROGRAM_CODE = "Loading...";
    this.PROGRAM_NAME = "Loading...";
    this.isAuthenicated = false;
    this.appSettings = {
      mdfnWebApiUrl: ""
    };

    this.apiUrl = null;
    this.baseApiUrl = environment.baseUrl;
    this.userViewModel = {
      CompanyName: "",
      FirstName: "",
      IsAuthenicated: false,
      LastName: "",
      OrganizaId: null,
      OrganizeName: "",
      RoleCode: "",
      UserName: "",
      UserId: null,
      Token: "",
      TokenExpirationDate: null
    };
  }

  isExpiredSession(): boolean {
    const userViewModel = JSON.parse(localStorage.getItem("userViewModel"));
    if (!userViewModel) {
      return true;
    }

    const Token = userViewModel.Token;

    if (Token === null || Token === undefined) {
      return true;
    }

    const isExpired: boolean = this.jwtHelperService.isTokenExpired(Token);
    return isExpired;
  }

  startSession() {
    const userViewModel: UserViewModel = JSON.parse(localStorage.getItem("userViewModel"));
    const token = userViewModel.Token;
    const decodedToken = this.jwtHelperService.decodeToken(token);

    this.userViewModel = {
      UserId: userViewModel.UserId,
      UserName: userViewModel.UserName,
      Token: token,
      FirstName: decodedToken.given_name,
      LastName: decodedToken.nameid,
      CompanyName: null,
      RoleCode: userViewModel.RoleCode,
      OrganizaId: userViewModel.OrganizaId,
      OrganizeName: userViewModel.OrganizeName,
      IsAuthenicated: true,
      TokenExpirationDate: this.jwtHelperService.getTokenExpirationDate(token)
    };

    localStorage.setItem("userViewModel", JSON.stringify(this.userViewModel));
  }
}
