import { Injectable } from "@angular/core";
import { HttpService } from "./http.service";
import { Resolve, Router, ActivatedRouteSnapshot } from "@angular/router";
import { SessionService } from "./session.service";
import { ProgramInfoViewModelResponse, ProgramInfoViewModel } from "../shared-models/program-info-view-model";
import { environment } from "src/environments/environment";
import { AlertService } from "./alert.service";

@Injectable({
  providedIn: "root"
})
export class ProgramInfoResolverService implements Resolve<ProgramInfoViewModel | any> {
  programInfoViewModel: ProgramInfoViewModel;
  apiUrl: string;
  constructor(private router: Router, private httpService: HttpService, private sessionService: SessionService, private alertService: AlertService) {
    this.apiUrl = environment.baseUrl;
  }

  resolve(route: ActivatedRouteSnapshot) {
    let PROGRAM_CODE = route.routeConfig.path;
    if (!PROGRAM_CODE) {
      this.router.navigate(["login"]);
      return false;
    }

    this.sessionService.startSession();
    this.initialzeProgramData(PROGRAM_CODE);
    this.getProgramInfo();
  }

  initialzeProgramData(PROGRAM_CODE: string) {
    this.programInfoViewModel = {
      PROGRAM_CODE: PROGRAM_CODE,
      PROGRAM_NAME: "",
      PROGRAM_LINK: "",
      ROLE_CODE: this.sessionService.userViewModel.RoleCode,
      ORGANIZE_ID: this.sessionService.userViewModel.OrganizaId
    };
  }

  getProgramInfo() {
    this.httpService.HttpPost<ProgramInfoViewModelResponse>(`${this.apiUrl}/ProgramInfo`, this.programInfoViewModel).subscribe(response => {
      if (response.Entity == null) {
        this.alertService.showWarningMessages("Warning!", ["Can't found program data"]);
        return false;
      }

      this.programInfoViewModel = response.Entity;
      this.sessionService.PROGRAM_CODE = this.programInfoViewModel.PROGRAM_CODE;
      this.sessionService.PROGRAM_NAME = this.programInfoViewModel.PROGRAM_NAME;
      this.sessionService.apiUrl = this.programInfoViewModel.PROGRAM_LINK;
    });
  }
}
