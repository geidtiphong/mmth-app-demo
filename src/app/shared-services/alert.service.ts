import { Injectable } from "@angular/core";

interface messageAlert {
  type: string;
  subject?: string;
  messages?: string;
  display: boolean;
}

@Injectable({
  providedIn: "root"
})
export class AlertService {
  messageAlert: messageAlert;
  subjectMessage: string;

  constructor() {
    this.messageAlert = {
      type: "danger",
      display: false
    };

    this.subjectMessage = "Operation successfully. ";
  }

  showWarningMessages(subject: string = "Error!", messages: string[] = []) {
    this.messageAlert.type = "warning";
    this.messageAlert.subject = subject;
    this.messageAlert.messages = "";

    if (messages.length == 1) {
      this.messageAlert.messages = messages[0] || null;
    } else {
      messages.forEach(value => (this.messageAlert.messages += `\r\n${value}`));
    }

    this.messageAlert.display = true;
  }

  showErrorMessages(subject: string = "Error!", messages: string[] = []) {
    this.messageAlert.type = "danger";
    this.messageAlert.subject = subject;
    this.messageAlert.messages = "";

    if (messages.length == 1) {
      this.messageAlert.messages = messages[0] || null;
    } else {
      messages.forEach(value => (this.messageAlert.messages += `\r\n${value}`));
    }

    this.messageAlert.display = true;
  }

  showSuccessMessages(subject: string = this.subjectMessage, messages: string[] = []) {
    this.messageAlert.type = "success";
    this.messageAlert.subject = subject;
    this.messageAlert.messages = "";

    if (messages.length == 1) {
      this.messageAlert.messages = messages[0] || null;
    } else {
      messages.forEach(value => (this.messageAlert.messages += `\r\n${value}`));
    }

    this.messageAlert.display = true;
  }
}
