import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectOrganizeComponent } from './select-organize.component';

describe('SelectOrganizeComponent', () => {
  let component: SelectOrganizeComponent;
  let fixture: ComponentFixture<SelectOrganizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectOrganizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectOrganizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
