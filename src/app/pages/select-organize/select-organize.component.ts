import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { HttpService, SessionService } from "src/app/shared-core-app/main-core-shared";

import { UserViewModel } from "../../shared-models/user-viewmodel";
import { OrganizeViewModel, OrganizeViewModelResponse } from "src/app/shared-models/organize-view-model";

@Component({
  selector: "app-select-organize",
  templateUrl: "./select-organize.component.html",
  styleUrls: ["./select-organize.component.scss"]
})
export class SelectOrganizeComponent implements OnInit {
  userViewModel: UserViewModel;
  organizeViewModel: OrganizeViewModel[];

  constructor(private router: Router, private httpService: HttpService, private sessionService: SessionService) {}

  ngOnInit() {
    this.organizeViewModel = [];
    this.getUser();
    this.getOrganize();
  }

  getUser() {
    let userViewModel = JSON.parse(localStorage.getItem("userViewModel")) || null;
    if (!userViewModel) {
      window.alert(`You are not authorization !`);
      this.router.navigate(["/login"]);
      return false;
    }

    this.userViewModel = userViewModel;
  }

  getOrganize() {
    let query = { roleCode: this.userViewModel.RoleCode };
    this.httpService.HttpPost<OrganizeViewModelResponse>(this.sessionService.appSettings.mdfnWebApiUrl + "select/getorganizesbyrole", query).subscribe(response => {
      this.organizeViewModel = response.Entity;
    });
  }

  onSelectedOrganize(event: OrganizeViewModel) {
    let key = typeof event != "undefined" ? event.OrganizeId : null;
    if (key == null) {
      return false;
    }

    this.userViewModel.OrganizaId = event.OrganizeId;
    this.userViewModel.OrganizeName = event.OrganizeName;
  }

  goToHome() {
    if (this.userViewModel.OrganizaId == null) {
      alert("Please select a organize");
      return false;
    }

    this.setSession();
    this.router.navigate(["/home"]);
  }

  setSession() {
    localStorage.setItem("userViewModel", JSON.stringify(this.userViewModel));
    this.sessionService.startSession();
  }
}
