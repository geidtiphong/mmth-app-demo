import { Component, OnInit } from "@angular/core";
import { HttpService } from "src/app/shared-core-app/main-core-shared";

@Component({
  selector: "app-test-api",
  templateUrl: "./test-api.component.html",
  styleUrls: ["./test-api.component.scss"]
})
export class TestApiComponent implements OnInit {
  apiUrl: string;
  results: any;
  constructor(public httpService: HttpService) {}

  ngOnInit() {}

  execute() {
    this.httpService.HttpPost(this.apiUrl, {}).subscribe(response => (this.results = response));
  }
}
