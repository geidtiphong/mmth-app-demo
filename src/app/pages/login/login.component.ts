import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { RoleViewModel } from "src/app/shared-models/role-view-model";
import { HttpService, SessionService } from "src/app/shared-core-app/main-core-shared";

import { UserViewModel, ResponseUserViewModel } from "../../shared-models/user-viewmodel";
import { RoleViewModelResponse } from "../../shared-models/role-view-model";
import { OrganizeViewModel, OrganizeViewModelResponse } from "src/app/shared-models/organize-view-model";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  userViewModel: UserViewModel;
  roleViewModel: RoleViewModel[];
  organizeViewModel: OrganizeViewModel[];

  SHOW_FORM_ORGANIZE: boolean;
  MUST_USE_ORGANIZE: boolean;

  constructor(private router: Router, private httpService: HttpService, private sessionService: SessionService) {}

  ngOnInit() {
    localStorage.clear();

    this.userViewModel = {
      UserName: "sirapob.s",
      Password: "123456",
      RoleCode: null,
      OrganizaId: null
    };

    this.roleViewModel = [];
    this.organizeViewModel = [];
    this.SHOW_FORM_ORGANIZE = false;
    this.MUST_USE_ORGANIZE = true;
    this.getRoles();
  }

  getRoles() {
    this.httpService.HttpGet<RoleViewModelResponse>(this.sessionService.appSettings.mdfnWebApiUrl + "authorization/getroles").subscribe(response => (this.roleViewModel = response.Entity));
  }

  login() {
    if (this.userViewModel.RoleCode == null) {
      window.alert("Please select a role!");
      return false;
    }

    let url = this.sessionService.appSettings.mdfnWebApiUrl + "authorization/login";
    this.httpService.HttpPost<ResponseUserViewModel>(url, this.userViewModel).subscribe(response => this.loginSuccess(response));
  }

  loginSuccess(response: ResponseUserViewModel) {
    this.userViewModel = response.Entity;
    this.getOrganize();
  }

  getOrganize() {
    let query = { roleCode: this.userViewModel.RoleCode };
    this.httpService.HttpPost<OrganizeViewModelResponse>(this.sessionService.appSettings.mdfnWebApiUrl + "select/getorganizesbyrole", query).subscribe(response => {
      this.organizeViewModel = response.Entity;

      if (this.organizeViewModel.length <= 0 && this.MUST_USE_ORGANIZE == true) {
        window.alert("Your system have not organize !");
      } else if (this.organizeViewModel.length == 1 && this.MUST_USE_ORGANIZE == true) {
        this.userViewModel.OrganizaId = this.organizeViewModel[0].OrganizeId;
        this.userViewModel.OrganizeName = this.organizeViewModel[0].OrganizeName;
        this.setSession();
        this.router.navigate(["/home"]);
      } else if (this.MUST_USE_ORGANIZE == false) {
        this.setSession();
        this.router.navigate(["/home"]);
      } else {
        this.SHOW_FORM_ORGANIZE = true;
      }
    });
  }

  onSelectedOrganize(event: OrganizeViewModel) {
    this.userViewModel.OrganizaId = event.OrganizeId;
    this.userViewModel.OrganizeName = event.OrganizeName;
  }

  goToHome() {
    if (this.userViewModel.OrganizaId == null) {
      window.alert("Please select a organize");
      return false;
    }

    this.setSession();
    this.router.navigate(["/home"]);
  }

  setSession() {
    localStorage.setItem("userViewModel", JSON.stringify(this.userViewModel));
    this.sessionService.startSession();
  }
}
