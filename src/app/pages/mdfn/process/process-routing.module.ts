import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ProgramInfoResolverService } from "src/app/shared-services/programinfo-resolver.service";

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProcessRoutingModule {}
