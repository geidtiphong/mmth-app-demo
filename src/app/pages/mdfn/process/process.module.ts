import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { NgbAccordionModule, NgbPaginationModule, NgbDatepickerModule, NgbModalModule, NgbTypeaheadModule } from "@ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedDirectiveModule } from "src/app/shared-directive/shared-directive.module";
import { ProcessRoutingModule } from "./process-routing.module";
import { SharedPipeModule } from "src/app/shared-pipe/shared-pipe.module";
import { TimeAmPmPipe } from "src/app/shared-pipe";

@NgModule({
  declarations: [],
  providers: [TimeAmPmPipe],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, NgbAccordionModule, NgbPaginationModule, NgbDatepickerModule, NgbModalModule, NgbTypeaheadModule, NgSelectModule, ProcessRoutingModule, SharedDirectiveModule, SharedPipeModule],
})
export class ProcessModule {}
