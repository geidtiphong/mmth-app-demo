import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import {
  BaseMasterComponent,
  IM0001M01Component,
  IM0001M02Component
} from "./index";
import {
  NgbAccordionModule,
  NgbPaginationModule,
  NgbDatepickerModule,
  NgbTabsetModule
} from "@ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";
import { FullCalendarModule } from "@fullcalendar/angular";
import { MasterRoutingModule } from "./master-routing.module";
import { SharedDirectiveModule } from "src/app/shared-directive/shared-directive.module";
import { SharedPipeModule } from "src/app/shared-pipe/shared-pipe.module";
import { TimeAmPmPipe } from "src/app/shared-pipe/time_am_pm.pipe";

@NgModule({
  declarations: [BaseMasterComponent, IM0001M01Component, IM0001M02Component],
  providers: [TimeAmPmPipe],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbAccordionModule,
    NgbPaginationModule,
    NgbDatepickerModule,
    NgbTabsetModule,
    NgSelectModule,
    MasterRoutingModule,
    SharedDirectiveModule,
    SharedPipeModule,
    FullCalendarModule
  ]
})
export class MasterModule {}
