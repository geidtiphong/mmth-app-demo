import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { IM0001M01Component, IM0001M02Component } from ".";

import { ProgramInfoResolverService } from "src/app/shared-services/programinfo-resolver.service";

const routes: Routes = [
  { path: "IM0001M01", component: IM0001M01Component },
  {
    path: "IM0001M02",
    component: IM0001M02Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterRoutingModule {}
