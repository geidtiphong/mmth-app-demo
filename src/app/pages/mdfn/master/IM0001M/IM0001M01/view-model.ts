import { BaseRequestModel, BaseViewModel } from "src/app/shared-models/base-request-model";

export interface ResponseViewModel extends BaseRequestModel {
  Entity: ViewModel[];
}

export interface RequestViewModel extends BaseRequestModel {
  title: string;
}

export interface ViewModel extends BaseViewModel {
  id: number;
  title: string;
  body: string;
}
