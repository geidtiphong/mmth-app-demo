import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { RequestViewModel, ResponseViewModel, ViewModel } from "./view-model";

import { HttpService, SessionService, AlertService, ConfirmationDialogService, DatetimeService } from "src/app/shared-core-app/main-core-shared";

@Component({
  selector: "app-IM0001M01",
  templateUrl: "./IM0001M01.component.html",
  styleUrls: ["./IM0001M01.component.scss"]
})
export class IM0001M01Component implements OnInit {
  viewModel: ViewModel[];
  requestViewModel: RequestViewModel;
  myForm: FormGroup;
  checkedRadio: number;
  searchCriteriaStorageKey: string;
  apiUrl:string;
  PROGRAM_CODE: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private sessionService: SessionService,
    private httpService: HttpService,
    private alertService: AlertService,
    private confirmationDialogService: ConfirmationDialogService,
    private datetimeService: DatetimeService
  ) {}

  ngOnInit() {
    this.checkedRadio = null;
    this.PROGRAM_CODE = "IM0001M01"
    this.apiUrl = "http://localhost:5000/api/IM2001M"
    this.searchCriteriaStorageKey = this.PROGRAM_CODE + "_searchCriteria";

    this.myForm = this.fb.group({
      title: [null]
    });

    this.initializeData();
    this.initializeSearch();
  }

  initializeData() {
    this.requestViewModel = {
      title: null,

      PageSize: 20,
      CurrentPageIndex: 1,
      TotalRows: 0,
      SortDirection: "",
      SortExpression: ""
    };

    this.viewModel = [];
  }

  initializeSearch() {
    let criteria = JSON.parse(localStorage.getItem(this.searchCriteriaStorageKey));
    if (criteria == null) {
      return false;
    }

    this.requestViewModel = criteria;
    this.myForm.patchValue(this.requestViewModel);
    this.executeSearch();
  }

  executeSearch() {
    this.checkedRadio = null;
    this.requestViewModel = Object.assign(this.requestViewModel, this.myForm.getRawValue());
    localStorage.setItem(this.searchCriteriaStorageKey, JSON.stringify(this.requestViewModel));
    this.httpService.HttpPost<ResponseViewModel>(`${this.apiUrl}/Search`, this.requestViewModel).subscribe(response => this.apiSuccess(response));
  }

  resetSearch() {
    this.initializeData();
    this.myForm.reset();
    this.checkedRadio = null;
    localStorage.removeItem(this.searchCriteriaStorageKey);
  }

  apiSuccess(response: ResponseViewModel) {
    this.viewModel = response.Entity;
    this.requestViewModel.TotalRows = response.TotalRows;
  }

  onEdit() {
    let id = this.viewModel[this.checkedRadio].id;
    this.router.navigate(["master/IM0001M02"], { queryParams: { id } });
  }

  doExport() {
    this.httpService.DownloadFileByPost<any>(`${this.apiUrl}/ExportExcel`, this.requestViewModel);
  }
}
