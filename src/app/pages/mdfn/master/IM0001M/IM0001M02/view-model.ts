import { BaseRequestModel } from "src/app/shared-models/base-request-model";

export interface ViewModel {
  id: number;
  title: string;
  body: string;
}

export interface ResponseViewModel extends ViewModel, BaseRequestModel {}

export interface RequestViewModel {}
