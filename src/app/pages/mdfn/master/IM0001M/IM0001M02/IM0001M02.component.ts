import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { ViewModel, ResponseViewModel } from "./view-model";
import { ConfirmationDialogService } from "src/app/shared-core-app/main-core-shared";
import {
  SessionService,
  HttpService,
  AlertService,
  DatetimeService
} from "src/app/shared-core-app/main-core-shared";

@Component({
  selector: "app-IM0001M02",
  templateUrl: "./IM0001M02.component.html",
  styleUrls: ["./IM0001M02.component.sass"]
})
export class IM0001M02Component implements OnInit {
  viewModel: ViewModel;
  myForm: FormGroup;
  submitted: boolean;
  apiUrl: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private sessionService: SessionService,
    private httpService: HttpService,
    private alertService: AlertService,
    private confirmationDialogService: ConfirmationDialogService,
    private datetimeService: DatetimeService
  ) {}

  ngOnInit() {
    this.apiUrl = "http://localhost:5000/api/IM2001M";
    this.myForm = this.fb.group({
      id: [0],
      title: [null, [Validators.required]],
      body: [null, [Validators.required]]
    });

    this.viewModel = this.myForm.getRawValue();
    this.initializeData();
  }

  get form() {
    return this.myForm.controls;
  }

  initializeData() {
    this.route.queryParams.subscribe(params => {
      this.viewModel.id = +params["id"] || 0;
      this.viewModel.id == 0 ? false : this.getItem();
      // if (this.viewModel.id != null) {
      //   this.getItem();
      // }
    });
  }

  getItem() {
    this.httpService
      .HttpPost<ResponseViewModel>(`${this.apiUrl}/SearchById`, this.viewModel)
      .subscribe(response => this.apiSuccess(response));
  }

  apiSuccess(response: ResponseViewModel) {
    this.viewModel = response.Entity;
    this.myForm.patchValue(this.viewModel);
  }

  openConfirmationSaveDialog() {
    this.submitted = true;

    if (this.myForm.invalid) {
      this.alertService.showWarningMessages("Warning!", [
        "Please verify the valid data!"
      ]);
      return false;
    }

    this.confirmationDialogService
      .confirm("Please confirm..", "Do you really want to save ?")
      .then(confirmed => (confirmed == true ? this.doSave() : false))
      .catch(err => console.log(err));
  }

  doSave() {
    this.viewModel = Object.assign(this.viewModel, this.myForm.getRawValue());
    let action = this.viewModel.id == null ? "Create" : "Update";
    this.httpService
      .HttpPost<ResponseViewModel>(`${this.apiUrl}/${action}`, this.viewModel)
      .subscribe(response => this.postSuccess(response));
  }

  postSuccess(response: ResponseViewModel) {
    this.alertService.showSuccessMessages(
      this.alertService.subjectMessage,
      response.ReturnMessage
    );
    this.doBack();
  }

  doBack() {
    this.sessionService.PROGRAM_CODE = `IM0001M01`;
    this.router.navigate(["master/IM0001M01"]);
  }
}
