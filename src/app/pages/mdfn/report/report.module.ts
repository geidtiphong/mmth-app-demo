import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { NgbAccordionModule, NgbPaginationModule, NgbDatepickerModule } from "@ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedDirectiveModule } from "src/app/shared-directive/shared-directive.module";
import { ReportRoutingModule } from "./report-routing.module";

@NgModule({
  declarations: [],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, NgbAccordionModule, NgbPaginationModule, NgbDatepickerModule, NgSelectModule, ReportRoutingModule, SharedDirectiveModule],
})
export class ReportModule {}
