import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { NgbAccordionModule, NgbPaginationModule, NgbDatepickerModule, NgbTabsetModule } from "@ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedDirectiveModule } from "src/app/shared-directive/shared-directive.module";
import { InterfaceRoutingModule } from "./interface-routing.module";

@NgModule({
  declarations: [],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, NgbAccordionModule, NgbPaginationModule, NgbDatepickerModule, NgbTabsetModule, NgSelectModule, InterfaceRoutingModule, SharedDirectiveModule],
})
export class InterfaceModule {}
