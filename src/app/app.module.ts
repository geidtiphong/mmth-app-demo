import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CurrencyPipe, HashLocationStrategy, LocationStrategy } from "@angular/common";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { NgbAlertModule, NgbModalModule, NgbDatepickerConfig } from "@ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTreeModule } from "@angular/material/tree";

import { HttpInterceptorService } from "./shared-services/http-interceptor.service";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./shared/header/header.component";
import { FooterComponent } from "./shared/footer/footer.component";
import { TreeMenuComponent } from "./shared/treemenu/treemenu.component";
import { ConfirmationDialogComponent } from "./components/confirmation-dialog/confirmation-dialog.component";
import { HomeComponent } from "./pages/home/home.component";
import { LoginComponent } from "./pages/login/login.component";

import { AppLayoutComponent } from "./_layout/app-layout/app-layout.component";
import { SiteLayoutComponent } from "./_layout/site-layout/site-layout.component";
import { routing } from "./app.routing";

import { TestApiComponent } from "./pages/test-api/test-api.component";
import { LoadingScreenComponent } from "./components/loading-screen/loading-screen.component";
import { SelectOrganizeComponent } from "./pages/select-organize/select-organize.component";

export class CustomDatePickerConfig extends NgbDatepickerConfig {
  navigation: "arrows";
}

@NgModule({
  declarations: [AppComponent, HeaderComponent, FooterComponent, TreeMenuComponent, HomeComponent, LoginComponent, SelectOrganizeComponent, ConfirmationDialogComponent, AppLayoutComponent, SiteLayoutComponent, LoadingScreenComponent, TestApiComponent],
  imports: [BrowserModule, BrowserAnimationsModule, FormsModule, HttpClientModule, routing, NgbAlertModule, NgbModalModule, MatSidenavModule, MatToolbarModule, MatTreeModule, NgSelectModule],
  providers: [
    CurrencyPipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: NgbDatepickerConfig, useClass: CustomDatePickerConfig }
  ],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmationDialogComponent]
})
export class AppModule {}
