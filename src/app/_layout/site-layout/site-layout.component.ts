import { Component, OnInit } from "@angular/core";
import { SessionService } from "../../shared-services/session.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-site-layout",
  templateUrl: "./site-layout.component.html",
  styleUrls: ["./site-layout.component.scss"]
})
export class SiteLayoutComponent implements OnInit {
  constructor(private sessionService: SessionService) {}

  ngOnInit() {
    this.sessionService.appSettings.mdfnWebApiUrl = environment.baseUrlLogin;
  }
}
