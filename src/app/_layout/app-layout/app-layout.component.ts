import { Component, OnInit } from "@angular/core";
import { Router, NavigationEnd, NavigationStart, NavigationCancel, NavigationError } from "@angular/router";
import { environment } from "src/environments/environment";
import { SessionService, AlertService } from "src/app/shared-core-app/main-core-shared";
import { LoadingScreenService } from "src/app/shared-services/loading-screen.service";

@Component({
  selector: "app-app-layout",
  templateUrl: "./app-layout.component.html",
  styleUrls: ["./app-layout.component.scss"],
})
export class AppLayoutComponent implements OnInit {
  today: string;
  version: string;
  app_name: string;
  app_footer: string;
  IS_OPEN: boolean;
  PROGRAM_CODE_PREV: string;

  constructor(public router: Router, public alertService: AlertService, public sessionService: SessionService, private loadingScreenService: LoadingScreenService) {
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.loadingScreenService.startLoading();
      }

      if (event instanceof NavigationEnd && event.urlAfterRedirects == "/home") {
        this.loadingScreenService.stopLoading();
        this.sessionService.SHOW_BREADCRUMB = false;
      } else if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
        this.loadingScreenService.stopLoading();
        this.sessionService.SHOW_BREADCRUMB = true;
      }
    });
  }

  ngOnInit() {
    this.sessionService.appSettings.mdfnWebApiUrl = environment.baseUrl;
    this.app_name = `iMEC System`;
    this.app_footer = `Copyright 2019, Mitsubishi Motors (Thailand) Co., Ltd., Released Version:`;
    this.version = `1.0-Beta`;
    this.today = `Loading...`;
    // this.sessionService.startSession();
    this.toggleClick();
    this.initialAlertdDataBox();

    setInterval(() => {
      let CURRENT_DATE = new Date();
      let timeFlag = CURRENT_DATE.getHours() >= 12 ? "PM" : "AM";
      this.today = CURRENT_DATE.toDateString() + " " + this.timePrefix(CURRENT_DATE.getHours()) + ":" + this.timePrefix(CURRENT_DATE.getMinutes()) + ":" + this.timePrefix(CURRENT_DATE.getSeconds()) + " " + timeFlag;
    }, 1000);
  }

  timePrefix(data) {
    if (data < 10) {
      data = `0${data}`;
    }

    return data;
  }

  initialAlertdDataBox() {
    this.alertService.messageAlert = {
      type: "danger",
      subject: "",
      messages: "",
      display: false,
    };
  }

  toggleClick() {
    this.IS_OPEN = !this.IS_OPEN;
  }

  logout() {
    localStorage.clear();
    this.router.navigate(["/login"]);
  }

  closeAlert(status: boolean) {
    this.alertService.messageAlert.display = status;
  }

  docloseAlert(event, status: boolean) {
    if (event.target.nodeName == "BUTTON") {
      this.alertService.messageAlert.display = status;
    }
  }
}
