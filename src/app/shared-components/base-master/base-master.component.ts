import { Component, OnInit } from "@angular/core";
import { ConfirmationDialogService } from "../../components/confirmation-dialog/confirmation-dialog.service";
import { SessionService, HttpService, AlertService, DatetimeService } from "src/app/shared-core-app/main-core-shared";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder } from "@angular/forms";

@Component({
  selector: "app-base-master",
  templateUrl: "./base-master.component.html",
  styleUrls: ["./base-master.component.sass"]
})
export class BaseMasterComponent implements OnInit {
  checkedRadio: number;
  searchCriteriaStorageKey: string;

  submitted: boolean;

  apiUrl: string;
  DisplayedColumns: any[];

  constructor(
    public router?: Router,
    public route?: ActivatedRoute,
    public fb?: FormBuilder,
    public sessionService?: SessionService,
    public httpService?: HttpService,
    public alertService?: AlertService,
    public confirmationDialogService?: ConfirmationDialogService,
    public datetimeService?: DatetimeService
  ) {}

  ngOnInit() {
    this.checkedRadio = 0;
    this.searchCriteriaStorageKey = this.sessionService.PROGRAM_CODE + "_searchCriteria";

    this.submitted = false;
  }
}
