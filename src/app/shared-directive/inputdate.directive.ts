import { Directive, HostListener, ElementRef, Output, EventEmitter } from "@angular/core";
import { formatDate } from "@angular/common";

@Directive({
  selector: "[appInputdate]"
})
export class InputdateDirective {
  @Output() DATE: EventEmitter<string> = new EventEmitter<string>();
  el: any;
  constructor(public elementRef: ElementRef) {
    this.el = elementRef.nativeElement;
  }

  @HostListener("blur", ["$event.target.value"])
  onblur(value) {
    let length = value.length;
    let mydate = null;
    // let input_date = null;

    if (length < 8) {
      mydate = this.el.value = null;
    } else {
      let year = value.substr(0, 4);
      let month = value.substr(4, 2);
      let days = value.substr(6, 2);
      let input_date = new Date(Number(year), Number(month), Number(days));
      let last_days_of_month = new Date(input_date.getFullYear(), input_date.getMonth(), 0);
      console.log("input month" + last_days_of_month);
      console.log("last days is " + last_days_of_month.getDate());
      console.log("input day is " + days);
      if (Number(month) > 0 && Number(month) <= 12 && Number(days) <= last_days_of_month.getDate()) {
        // let input_date = new Date(Number(year), Number(month) - 1, Number(days));
        mydate = formatDate(input_date, "yyyy-MM-dd", "en");
        this.el.value = mydate;
        console.log(mydate);
      }
    }

    this.DATE.emit(mydate);
  }

  @HostListener("focus", ["$event.target.value", "$event"])
  onfocus(value, event) {
    const NUMBERS_ONLY = String(value).replace(/[-]/g, "");
    this.el.value = NUMBERS_ONLY;
    this.el.select();
  }

  @HostListener("keyup", ["$event"])
  onkeydown(event) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    let length = this.el.value.length;

    if (!pattern.test(inputChar) || length > 8) {
      return false;
    }
  }
}
