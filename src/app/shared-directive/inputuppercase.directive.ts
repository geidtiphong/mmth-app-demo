import { Directive, HostListener, ElementRef, Output, EventEmitter } from "@angular/core";

@Directive({
  selector: "[appInputuppercase]"
})
export class InputuppercaseDirective {
  @Output() DATA: EventEmitter<string> = new EventEmitter<string>();
  el: any;
  constructor(public elementRef: ElementRef) {
    this.el = elementRef.nativeElement;
  }

  @HostListener("keyup", ["$event.target.value"])
  onkeydown(value) {
    let length = value.length;
    let result = "";
    this.el.value = result;

    if (length > 0) {
      result = String(value).toUpperCase();
      this.el.value = result;
    }

    this.DATA.emit(result);
  }
}
