import { Directive, ElementRef, HostListener, Input } from "@angular/core";
import { CurrencyPipe } from "@angular/common";

@Directive({
  selector: "[appInputcurrency]",
})
export class InputcurrencyDirective {
  @Input() fraction: number;
  el: any;
  constructor(public elementRef: ElementRef, private currencyPipe: CurrencyPipe) {
    this.el = elementRef.nativeElement;
  }

  @HostListener("focus", ["$event.target.value", "$event"])
  onfocus(value, event) {
    const NUMBERS_ONLY = String(value).replace(/[,]/g, "");
    this.el.value = NUMBERS_ONLY;
    this.el.select();
  }

  @HostListener("blur", ["$event.target.value"])
  onblur(value) {
    const NUMBERS_ONLY = String(value).replace(/[,]/g, "");
    if (this.fraction == 2) {
      this.el.value = this.currencyPipe.transform(NUMBERS_ONLY, "", "", "1.2-2");
    } else if (this.fraction == 4) {
      this.el.value = this.currencyPipe.transform(NUMBERS_ONLY, "", "", "1.4-4");
    } else if (this.fraction == 5) {
      this.el.value = this.currencyPipe.transform(NUMBERS_ONLY, "", "", "1.5-5");
    } else {
      this.el.value = this.currencyPipe.transform(NUMBERS_ONLY, "", "", "1.0");
    }
  }

  @HostListener("keypress", ["$event"])
  onkeydown(event) {
    const pattern = /[0-9\+\.\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      return false;
    }
  }

  @HostListener("keypress", ["$event"])
  numberAndDot(event) {
    const pattern = /[0-9\.\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      return false;
    }
  }
}
