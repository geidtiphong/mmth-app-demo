import { Directive, HostListener, ElementRef, Output, EventEmitter } from "@angular/core";

@Directive({
  selector: "[appInputtime]"
})
export class InputtimeDirective {
  @Output() TIME: EventEmitter<string> = new EventEmitter<string>();
  el: any;
  constructor(public elementRef: ElementRef) {
    this.el = elementRef.nativeElement;
  }

  @HostListener("blur", ["$event.target.value"])
  onblur(value) {
    let length = value.length;
    let time = null;
    if (length < 4) {
      time = this.el.value = null;
    } else {
      let start_time = value.substr(0, 2);
      let end_time = value.substr(2, 2);

      if (Number(start_time) <= 23 && Number(end_time) <= 59) {
        time = this.el.value = `${start_time}:${end_time}`;
      }
    }

    this.TIME.emit(time);
  }

  @HostListener("focus", ["$event.target.value", "$event"])
  onfocus(value, event) {
    const NUMBERS_ONLY = String(value).replace(/[:]/g, "");
    this.el.value = NUMBERS_ONLY;
    this.el.select();
  }

  @HostListener("keypress", ["$event"])
  onkeydown(event) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    let length = this.el.value.length;

    if (!pattern.test(inputChar) || length > 4) {
      return false;
    }
  }
}
