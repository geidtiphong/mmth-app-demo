import { NgModule } from "@angular/core";
import { InputcurrencyDirective, InputtimeDirective, InputFilesDirective, InputuppercaseDirective, InputTimeWithSecondDirective, InputdateDirective } from ".";

@NgModule({
  declarations: [InputcurrencyDirective, InputtimeDirective, InputFilesDirective, InputuppercaseDirective, InputdateDirective, InputTimeWithSecondDirective],
  exports: [InputcurrencyDirective, InputtimeDirective, InputFilesDirective, InputuppercaseDirective, InputdateDirective, InputTimeWithSecondDirective],
})
export class SharedDirectiveModule {}
