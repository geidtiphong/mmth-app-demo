import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./pages/home/home.component";
import { LoginComponent } from "./pages/login/login.component";
import { AppLayoutComponent } from "./_layout/app-layout/app-layout.component";
import { SiteLayoutComponent } from "./_layout/site-layout/site-layout.component";
import { TestApiComponent } from "./pages/test-api/test-api.component";
import { AuthGuardService } from "./shared-services/auth-guard.service";
import { SelectOrganizeComponent } from "./pages/select-organize/select-organize.component";

const appRoutes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  {
    path: "",
    component: AppLayoutComponent,
    // canActivate: [AuthGuardService],
    children: [
      { path: "home", component: HomeComponent },
      { path: "master", loadChildren: () => import('./pages/mdfn/master/master.module').then(m => m.MasterModule) },
      { path: "process", loadChildren: () => import('./pages/mdfn/process/process.module').then(m => m.ProcessModule) },
      { path: "report", loadChildren: () => import('./pages/mdfn/report/report.module').then(m => m.ReportModule) },
      { path: "interface", loadChildren: () => import('./pages/mdfn/interface/interface.module').then(m => m.InterfaceModule) }
    ]
  },
  {
    path: "test-api",
    component: TestApiComponent
  },
  {
    path: "",
    component: SiteLayoutComponent,
    children: [{ path: "login", component: LoginComponent }, { path: "select", component: SelectOrganizeComponent }]
  },

  // { path: "**", redirectTo: "login" }
  { path: "**", redirectTo: "home" }
];

export const routing = RouterModule.forRoot(appRoutes);
