import { Pipe, PipeTransform } from "@angular/core";
import { formatDate } from "@angular/common";
import { DatetimeService } from "../shared-core-app/main-core-shared";
@Pipe({
  name: "time_am_pm"
})
export class TimeAmPmPipe implements PipeTransform {
  constructor(public datetimeService: DatetimeService) {}
  transform(val: string): any {
    let current_date = this.datetimeService.CURRENT_DATE.split("-");
    let time = val.split(":");
    let datetime = new Date(Number(current_date[0]), Number(current_date[1]), Number(current_date[2]), Number(time[0]), Number(time[1]), Number(time[2] || '00'));
    return formatDate(datetime, "hh:mm:ss a", "en");
  }
}
