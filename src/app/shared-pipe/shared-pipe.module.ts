import { NgModule } from "@angular/core";
import { TimeAmPmPipe } from ".";

@NgModule({
  declarations: [TimeAmPmPipe],
  exports: [TimeAmPmPipe],
})
export class SharedPipeModule {}
