import { NestedTreeControl } from "@angular/cdk/tree";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { MatTreeNestedDataSource } from "@angular/material/tree";
import { SessionService, HttpService } from "src/app/shared-core-app/main-core-shared";
import { environment } from "src/environments/environment";
import { BaseRequestModel } from "src/app/shared-models/base-request-model";
import { FoodNode } from "src/app/shared-models/food-node-model";

const MENU = [
  {
    Name: "iMEC",
    Link: null,
    ProgramCode: null,
    Children: [
      {
        Name: "Master",
        Link: null,
        ProgramCode: null,
        Children: [
          {
            Name: "Master Demo",
            Link: "http://localhost:8001/api/im0001m",
            ProgramCode: "IM0001M01",
            LinkProgramCode: 'master/IM0001M01',
            Children: [],
          },
        ],
      },
    ],
  },
];

@Component({
  selector: "app-treenode",
  templateUrl: "treemenu.component.html",
  styleUrls: ["treemenu.component.scss"],
})
export class TreeMenuComponent implements OnInit {
  treeControl = new NestedTreeControl<FoodNode>(node => node.Children);
  dataSource = new MatTreeNestedDataSource<FoodNode>();
  dataSourceMaster: FoodNode[];
  ORGANIZE_ID: number;
  RoleCode: string;
  QUERY_SELECTBOX: any = {};
  apiUrl: string;
  IS_LOADING: boolean;
  @ViewChild("tree", { static: false }) tree;

  constructor(private router: Router, public sessionService: SessionService, public httpService: HttpService) {}

  ngOnInit() {
    this.IS_LOADING = true;
    this.dataSource.data = [];
    this.dataSourceMaster = [];
    this.apiUrl = environment.baseUrlLogin;
    this.ORGANIZE_ID = this.sessionService.userViewModel.OrganizaId || 4;
    this.RoleCode = this.sessionService.userViewModel.RoleCode || "IM_U01_R01";
    this.QUERY_SELECTBOX = { OrganizeId: this.ORGANIZE_ID, RoleCode: this.RoleCode };
    this.GetMenuByOrganize();

    let node: FoodNode = JSON.parse(localStorage.getItem("node")) || null;
    if (node) {
      this.onSelectedPage(node);
    }
  }

  collapseAll() {
    this.treeControl.collapseAll();
  }

  expandAll() {
    this.treeControl.dataNodes = this.dataSource.data;
    this.treeControl.expandAll();
  }

  GetMenuByOrganize() {
    this.IS_LOADING = false;
    this.dataSource.data = MENU;
    this.dataSourceMaster = MENU;
  }
  // GetMenuByOrganize() {
  //   this.httpService.HttpPost<BaseRequestModel>(`${this.apiUrl}Authorization/GetMenusByOrganizeRole`, this.QUERY_SELECTBOX).subscribe(response => {
  //     this.IS_LOADING = false;
  //     let Entity: FoodNode = response.Entity;

  //     Entity.Children.forEach((value: FoodNode) => {
  //       let lowcaseProgramname = value.Name.toLowerCase();
  //       value.Children.forEach((node: FoodNode) => {
  //         node.MainName = value.Name;
  //         node.LinkProgramCode = `${lowcaseProgramname}/${node.ProgramCode}`;
  //       });
  //     });

  //     let dataSource: FoodNode[] = [
  //       {
  //         Name: Entity.Name,
  //         Children: Entity.Children,
  //       },
  //     ];

  //     this.dataSource.data = dataSource;
  //     this.dataSourceMaster = dataSource;
  //   });
  // }

  hasChild = (_: number, node: FoodNode) => !!node.Children && node.Children.length > 0;

  onSelectedPage(node: FoodNode, clicklink: boolean = false) {
    this.sessionService.apiUrl = node.Link;
    this.sessionService.MAIN_PROGRAM_NAME = node.MainName;
    this.sessionService.PROGRAM_CODE = node.ProgramCode;
    this.sessionService.PROGRAM_NAME = `${node.Name}`;
    console.log(node)
    if (clicklink) {
      this.router.navigate([node.LinkProgramCode]);
    }

    localStorage.setItem("node", JSON.stringify(node));
  }

  filterChanged(filterText: string) {
    let data: FoodNode[] = this.dataSourceMaster;
    let dataSource: FoodNode[] = [
      {
        Name: data[0].Name,
        Link: null,
        Children: [],
        ProgramCode: null,
      },
    ];

    this.dataSource.data = null;

    if (filterText && data.length > 0) {
      data.forEach(value =>
        value.Children.filter((nodeData, key) => {
          let nodeProgramMain: FoodNode = {
            Link: null,
            Name: nodeData.Name,
            Children: [],
            ProgramCode: null,
          };

          dataSource[0].Children.push(nodeProgramMain);

          let nodeLinkProgram = nodeData.Children.filter(nodeLink => {
            let programName = nodeLink.Name;
            let ProgramCode = nodeLink.ProgramCode;
            if (programName.toLowerCase().search(filterText.toLowerCase()) > -1 || ProgramCode.search(filterText) > -1) {
              return nodeLink;
            }
          });

          dataSource[0].Children[key].Children = nodeLinkProgram;
        })
      );

      let filterData: FoodNode[] = dataSource;

      this.dataSource.data = filterData;
      this.treeControl.dataNodes = this.dataSource.data;
      this.treeControl.expandAll();
    } else {
      this.dataSource.data = data;
      this.treeControl.collapseAll();
    }
  }
}
